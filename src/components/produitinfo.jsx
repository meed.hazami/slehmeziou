import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import TableBody from "@material-ui/core/TableBody";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

function createData(number, item, qty, price) {
  return { number, item, qty, price };
}

const rows = [
  createData(1, "Apple", 5, 3),
  // createData(2, "Orange", 2, 2),
  // createData(3, "Grapes", 3, 1),
  // createData(4, "Tomato", 2, 1.6),
  // createData(5, "Mango", 1.5, 4)
];

export default function Produitinfo(props) {
  const [count, setCount] = useState(0);
  // const data = props.location.state
  const { text ,img } = useParams();

  return (
    <div style={{width : "90%" , margin :"auto" }}>
      {/* {console.log("data" , `/static/media/${img}`)} */}
      <img style={{height : "auto" , width : "70%" , marginTop : "5px"}} src={`/static/media/${img}`} alt="logo" />
      <h3 style={{color :   "#6C6C70" , fontFamily: "Century Gothic"}}>{text}</h3>
      <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow >
            <TableCell style={{ color : "#6C6C70"}} >S.No</TableCell>
            <TableCell style={{ color : "#6C6C70"}}  align="right">Item</TableCell>
            <TableCell style={{ color : "#6C6C70"}}  align="right">Quantity&nbsp;(kg)</TableCell>
            <TableCell style={{ color : "#6C6C70"}}  align="right">Price&nbsp;($)</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.number}>
              <TableCell style={{ color : "#6C6C70"}}  component="th" scope="row">
                {row.number}
              </TableCell>
              <TableCell style={{ color : "#6C6C70"}}  align="right">{row.item}</TableCell>
              <TableCell style={{ color : "#6C6C70"}}  align="right">{row.qty}</TableCell>
              <TableCell style={{ color : "#6C6C70"}}  align="right">{row.price}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
  );
}

//Produitinfo
