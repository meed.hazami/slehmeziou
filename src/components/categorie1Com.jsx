import React, { Component } from "react";
import { Link } from "react-router-dom";
import Produit from "./produit";
import Pro1 from "../assets/images/pro1.jpg";
import Pro2 from "../assets/images/pro2.jpg";
import Pro3 from "../assets/images/pro3.jpg";
import Pro4 from "../assets/images/pro4.jpg";

import TextField from "@mui/material/TextField";
import InputAdornment from "@mui/material/InputAdornment";
import SearchIcon from "@mui/icons-material/Search";

import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import "./styleNav.css";

class Categoriecom1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: [0 , 100],
      data: {
        img: Pro1.slice(14),
        text: "AB Produit1",
      },
      isLoaded: false,
      filterVal: "",
      filterRes: [
        { imgPro: Pro1, title: "CD Produit2" , prix : 10 },
        { imgPro: Pro2, title: "EF Produit3" , prix : 20},
        { imgPro: Pro3, title: "RT Produit4" , prix : 30},
        { imgPro: Pro4, title: "YJ Produit5" , prix : 40},
        { imgPro: Pro1, title: "SD Produit6" , prix : 50},
        { imgPro: Pro2, title: "KI Produit7" , prix : 15},
        { imgPro: Pro3, title: "PL Produit8" , prix : 25},
        { imgPro: Pro4, title: "ML Produit9" , prix : 65},
        { imgPro: Pro1, title: "MR Produit10" , prix : 75},
        { imgPro: Pro2, title: "MI Produit11" , prix : 85},
        { imgPro: Pro3, title: "MN Produit12" , prix : 95},
      ],
      totalUser: [
        { imgPro: Pro1, title: "CD Produit2" , prix : 10 },
        { imgPro: Pro2, title: "EF Produit3" , prix : 20},
        { imgPro: Pro3, title: "RT Produit4" , prix : 30},
        { imgPro: Pro4, title: "YJ Produit5" , prix : 40},
        { imgPro: Pro1, title: "SD Produit6" , prix : 50},
        { imgPro: Pro2, title: "KI Produit7" , prix : 15},
        { imgPro: Pro3, title: "PL Produit8" , prix : 25},
        { imgPro: Pro4, title: "ML Produit9" , prix : 65},
        { imgPro: Pro1, title: "MR Produit10" , prix : 75},
        { imgPro: Pro2, title: "MI Produit11" , prix : 85},
        { imgPro: Pro3, title: "MN Produit12" , prix : 95},
      ],
    };
  }

  searchChange = (event) => {
    if (event.target.value) {
      let filterResult = this.state.filterRes
        .filter((res) => res)
        .map((el) => {
          return el.title
            .toUpperCase()
            .includes(event.target.value.toUpperCase())
            ? el
            : "" 
        });
      this.setState({
        filterVal: event.target.value,
        filterRes: filterResult,
        isLoaded: true,
      });
    } else {
      this.setState({
        filterVal: event.target.value,
        filterRes: this.state.totalUser,
        isLoaded: true,
      });
    }
  };

   handleChange = (event, newValue) => {
      this.setState({value : newValue})

        let filterResult = this.state.totalUser
          .filter((res) => res.prix <= this.state.value[1] && res.prix >= this.state.value[0])
          .map((el) => {
            return el
          });
        this.setState({
          filterRes: filterResult,
          isLoaded: true,
        });

    };

  render() {
    const { isLoaded, filterVal, filterRes, totalUser } = this.state;
    const newTo = {
      pathname: `/produitInfo/${this.state.data && this.state.data.text}/${
        this.state.data && this.state.data.img
      }`,
    };

    // const [value, setValue] = React.useState<number[]>([0, 137]);




    return (
      <section className="flexFilter">

      <section className="Filter">
        <h2>Filter</h2>
        <div style={{display: "flex" , justifyContent : "center" , alignItems: "start"}}>
        <Box sx={{ width: "80%" }}>
        <Slider
           getAriaLabel={() => 'Temperature range'}
           value={this.state.value}
           onChange={this.handleChange}
          //  getAriaValueText={valuetext}
           valueLabelDisplay="auto"
          />
        </Box>
        </div>
      </section>
      <section className="ListProduit">
        <section
          style={{
            display: "flex",
            justifyContent: "space-around",
            flexWrap: "wrap",
          }}
        >
          <span className="vide"></span>
          <span
            style={{
              fontFamily: "Century Gothic Bold",
              fontSize: "1.4rem",
              lineHeight: "1.2",
              letterSpacing: "2px",
              color: "#6C6C70",
              textTransform: "uppercase",
            }}
          >
            Produit 
          </span>

          {/* <input
            type="text"
            name="search"
            value={filterVal}
            onChange={this.searchChange}
            placeholder="enter the name"
          /> */}

         <div >
          <TextField
            autoComplete="off"
            id="outlined-basic"
            label="Recherche"
            name="search"
            value={filterVal}
            onChange={this.searchChange}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
            size="small"
            variant="outlined"
            fullWidth
          />
        </div>
        </section>

        <div
          style={{
            display: "flex",
            justifyContent: "space-around",
            alignItems: "center",
            flexWrap: "wrap",
          }}
        >
          {filterRes.map((val, index) => (
            <Link key={index} to={newTo}>
              {val.imgPro !== undefined ? (
                <Produit imgPro={val.imgPro} title={val.title} prix={val.prix} />
              ) : null}
            </Link>
          ))}
        </div>
      </section>
      </section>
    );
  }
}

export default Categoriecom1;
