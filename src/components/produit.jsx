import React, { Component } from 'react';


class Produit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date()
        };
      }
    render() {
        const {imgPro , title , prix} = this.props
        return (
            <div>
                <img style={{height : "200px" , width : "auto" , marginTop: "5px"}} src={imgPro} alt="logo" />
                <p style={{fontFamily: 'Century Gothic', color: '#6C6C70'}}>{title}</p>
                <p style={{fontFamily: 'Century Gothic', color: '#6C6C70'}}>{prix}</p>
            </div>
        );
    }
}

export default Produit;
