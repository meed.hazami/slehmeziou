import React, { Component } from "react";

export default class Section11 extends Component {
  render() {
    return (
      <div
        style={{
          //   width: "50%",
          //   margin: "auto",
          paddingTop: "50px",
          paddingBottom: "50px",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-around",
            flexWrap : "wrap"
          }}
        >
          <div
            id="ABOUTUS"
            style={{
              width: "400px",
              paddingTop: "50px",
            }}
          >
            <h3 >Qui somme nous</h3>
            <p
              style={{
                color: "rgb(93, 93, 105)",
                marginTop: "0.58824rem",
                fontWeight: "400",
                letterSpacing: "normal",
                fontSize: "1.41176rem",
                lineHeight: "1.94118rem",
              }}
            >
              better online business that’s up and running in less time, with
              less cost. With Essentials, get an ecommerce platform that won’t
              hold you back today or in the future—with your choice of the best
              features and functionality to showcase your products and manage
              your business.
            </p>
          </div>
          <div
            style={{
              width: "500px",
              paddingTop: "50px",
            }}
          >
            <div className="video-responsive">
              <iframe
                src={`https://www.youtube.com/embed/wDhysutq8-w?autoplay=1&mute=1`}
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
                title="Embedded youtube"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
