import React, { Component } from 'react'
// import Section0 from "./section0"
import Section1 from "./section1"
import Section11 from "./section11"
import Section2 from "./section2"
import Section3 from "./section3"
import Section4 from "./section4"
import Map from "./section5"


export default class Home extends Component {
  render() {
    return (
      <>
      {/* <Section0/> */}
      <Section1/>
      <Section11/>
      <Section2/>
      <Section3/>
      <Section4/>
      <Map zoom={15} center={{lat:34.745818,lng:10.760035}}/>

      </>
    )
  }
}
