import React, { Component } from 'react'
import FacebookIcon from "@mui/icons-material/Facebook";
import AddIcCallIcon from "@mui/icons-material/AddIcCall";
import InstagramIcon from '@mui/icons-material/Instagram';
import EmailIcon from "@mui/icons-material/Email";
import image6 from "../../assets/images/eryh.png";
import logo from "../../assets/images/logo.PNG";
import FloatingWhatsApp from 'react-floating-whatsapp'
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';

export default class Section6 extends Component {
  render() {
    return (
      <section
      style={{
        backgroundColor: "#111111",
        width: "100%",
        minHeight: "400px",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
          width: "100%",
          flexWrap: "wrap",
          minHeight: "300px",
        }}
      >

        <section
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection : "column",
            height: "100px",
            marginTop : "100px",
          }}
        >
          <h3  style={{ color: "#535891" }}>SEMPACO</h3>
          <img src={image6} style={{width : "80px" , height : "auto"}}/>
        </section>

        <div style={{ height: "20px" , height: "100px" , marginTop : "50px"}}>
          <h3
            style={{
              color: "#535891",
            }}
          >
            Contactez-nous : 
          </h3>
          <ul style={{ textAlign: "start", listStyleType: "none" }}>
            <li style={{  paddingLeft: "50px" }}>
            <a style={{ color: "#535891" , textDecoration : "none" }} href="tel:+21674443620">
                <AddIcCallIcon /> +216 74 444 444
            </a>
            </li>
            <li style={{ color: "#535891" , paddingLeft: "50px" }}>
            <a style={{ color: "#535891"  , textDecoration : "none" }} href="tel:+21653416577">
                <AddIcCallIcon /> +216 22 222 222
            </a>
            </li>
            <li style={{ color: "#535891" , paddingLeft: "50px" }}>

              <a style={{ color: "#535891"  , textDecoration : "none" }} href="mailto:med.sleh.meziou@gmail.com">
                <EmailIcon /> med.sleh.meziou@gmail.com 
              </a>

            </li>
          </ul>
        </div>

        <div style={{ height: "20px" , height: "100px" , marginTop : "50px"}}>
          <h3 style={{ color: "#535891" }}>
            Réseaux sociaux :
          </h3>
          <ul style={{ textAlign: "start", listStyleType: "none" }}>
            <li style={{ color: "#fff"}}>
              <a style={{ color: "#535891" , textDecoration : "none" }} target="_blank" href="https://www.facebook.com/">
                <FacebookIcon />  facebook
              </a>
            </li>
            <li style={{ color: "#535891" }} >
              <a style={{ color: "#535891" , textDecoration : "none"}} target="_blank" href="https://www.linkedin.com/">
                <InstagramIcon />{" "} Instagram
              </a>
            </li>
          </ul>
          <h3 style={{ color: "#535891" , textAlign : "start" }}>
            RIB Banque: 
          </h3>
          <ul style={{ textAlign: "start", listStyleType: "none" }}>
            <li style={{ color: "#535891" }}>
                <AccountBalanceIcon />
                <a style={{ color: "#535891" }}  href="#">{" "}
                 12345 1234 123456789 12
                 </a>
            </li>
          </ul>
        </div>



        <FloatingWhatsApp 
                          phoneNumber="+21629657198" 
                          accountName="SEMPACO"
                          avatar={logo} 
                          notification={true}
                          chatMessage="bienvenue au SEMPACO, Nos conseillers sont a votre disposition du lundi au samedi"
        >
        </FloatingWhatsApp>


      </div>
      <h8 style={{color: "#fff"  }} id="CONTACT">SITE WEB RÉALISÉ PAR 
          <h8 style={{ color: "#fff" }}>  SEMPACO</h8>
      | © 2022. TOUS DROITS RÉSERVÉS</h8>
    </section>
    )
  }
}
