import React, { Component } from "react";
import boxAvatar1 from "../../assets/images/03-ValProp1.jpg";
import Button from "@mui/material/Button";

export default class Section4 extends Component {
  render() {
    return (
      <div>
        <section style={{ maxWidth: "80%", margin: "auto" }}>
          <h1
            style={{
              marginTop: "100px",
              color: "#121118",
              fontWeight: "700",
              letterSpacing: "normal",
              fontSize: "2.41176rem",
              lineHeight: "2.94118rem",
            }}
          >
            Call Support
          </h1>
          <p
            style={{
              color: "rgb(93, 93, 105)",
              marginTop: "0.58824rem",
              fontWeight: "400",
              letterSpacing: "normal",
              fontSize: "1.41176rem",
              lineHeight: "1.94118rem",
            }}
          >
            better online business that’s up and running in less time, with less
            cost. With Essentials, get an ecommerce platform that won’t hold you
            back today or in the future—with your choice of the best features
            and functionality to showcase your products and manage your
            business.
          </p>
          <hr
            style={{
              border: "solid #e1e6eb",
              borderWidth: "1px 0 0",
              clear: "both",
              margin: "1.5rem 0 1.44118rem",
              width: "80%",
              marginTop: "50px",
              marginBottom: "50px",
            }}
          ></hr>
        </section>

        <section
          style={{
            marginTop: "50px",
            display: "flex",
            justifyContent: "space-around",
            alignItems: "center",
            flexWrap: "wrap",
          }}
        >
          <img
            style={{ width: "300px", heigth: "auto" }}
            src={boxAvatar1}
            alt="React Logo"
          />
          <div className="paragraphhome">
            <h1
              style={{
                textAlign: "start",
                fontWeight: "700",
                letterSpacing: "normal",
                fontSize: "2rem",
                lineHeight: "2.64706rem",
              }}
            >
              Simplify ecommerce management
            </h1>
            <p
              style={{
                textAlign: "center",
                textAlign: "start",
                marginTop: "0.58824rem",
                color: "rgb(93, 93, 105)",
              }}
            >
              Take the guesswork out of managing your new or growing online
              business.
            </p>
            <ul>
              <li
                style={{
                  textAlign: "start",
                  lineHeight: "2rem",
                  color: "rgb(93, 93, 105)",
                }}
              >
                Improve planning with inventory management tools
              </li>
              <li
                style={{
                  textAlign: "start",
                  lineHeight: "2rem",
                  color: "rgb(93, 93, 105)",
                }}
              >
                Craft personalized shopping experiences with customer groups
              </li>
              <li
                style={{
                  textAlign: "start",
                  lineHeight: "2rem",
                  color: "rgb(93, 93, 105)",
                }}
              >
                Choose from leading shipping and payment providers to find your
                best fit solutions, without penalties
              </li>
            </ul>
          </div>
        </section>

        <hr
          style={{
            border: "solid #e1e6eb",
            borderWidth: "1px 0 0",
            clear: "both",
            margin: "1.5rem 0 1.44118rem",
            width: "80%",
            margin: "auto",
            marginTop: "50px",
            marginBottom: "50px",
          }}
        ></hr>

        <section
          style={{
            marginTop: "50px",
            display: "flex",
            justifyContent: "space-around",
            alignItems: "center",
            flexWrap: "wrap",
          }}
        >
          <div className="paragraphhome">
            <h1
              style={{
                textAlign: "start",
                fontWeight: "700",
                letterSpacing: "normal",
                fontSize: "2rem",
                lineHeight: "2.64706rem",
              }}
            >
              Simplify ecommerce management
            </h1>
            <p
              style={{
                textAlign: "center",
                textAlign: "start",
                marginTop: "0.58824rem",
                color: "rgb(93, 93, 105)",
              }}
            >
              Take the guesswork out of managing your new or growing online
              business.
            </p>
            <ul>
              <li
                style={{
                  textAlign: "start",
                  lineHeight: "2rem",
                  color: "rgb(93, 93, 105)",
                }}
              >
                Improve planning with inventory management tools
              </li>
              <li
                style={{
                  textAlign: "start",
                  lineHeight: "2rem",
                  color: "rgb(93, 93, 105)",
                }}
              >
                Craft personalized shopping experiences with customer groups
              </li>
              <li
                style={{
                  textAlign: "start",
                  lineHeight: "2rem",
                  color: "rgb(93, 93, 105)",
                }}
              >
                Choose from leading shipping and payment providers to find your
                best fit solutions, without penalties
              </li>
            </ul>
          </div>
          <img
            style={{ width: "300px", heigth: "auto" }}
            src={boxAvatar1}
            alt="React Logo"
          />
        </section>
        <div
          style={{
            width: "40%",
            margin: "auto",
            marginTop: "50px",
            marginBottom: "50px",
          }}
        >
          <Button
            fullWidth={true}
            size="large"
            variant="contained"
            disableElevation
          >
            More information
          </Button>
        </div>
      </div>
    );
  }
}
