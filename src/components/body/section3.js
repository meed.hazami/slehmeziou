import React, { Component } from 'react'
import prod1 from "../../assets/images/prod1.webp";
import prod2 from "../../assets/images/prod2.webp";
import prod3 from "../../assets/images/prod3.webp";
import prod4 from "../../assets/images/prod4.webp";
import prod5 from "../../assets/images/prod5.webp";
import prod6 from "../../assets/images/prod6.webp";

export default class Section3 extends Component {
  render() {
    return (
      <section>

      <div style={{marginTop: "50px" ,display : "flex", alignItems: "center", justifyContent:"center", flexDirection: "column", width : "100%" , backgroundColor: "#121118" ,paddingBottom: "0.94118rem"}}>
          <h2 style={{marginTop: "30px" , color : "#fff"}}>Recommender</h2>
      </div>
     
      <section style={{ width : "80%" ,
                        margin : "auto" , 
                        marginTop : "20px",
                        display: "flex",
                        justifyContent: "space-between",
                        flexWrap: "wrap"
                        }}>

      <div>
          <div style={{width : "300px" , height : "480px" }}>
            <img style={{height : "auto" , width : "300px"}} src={prod1} alt="logo" />
            <p style={{ width : "300px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>SoPRO UV/LED Color Hybrid Gel *4 HEMA FREE 7 g</p>
            <p style={{width : "300px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>€9.49 </p>
          </div>
      </div>

      <div>
          <div style={{width : "300px" , height : "480px" }}>
            <img style={{height : "auto" , width : "300px"}} src={prod2} alt="logo" />
            <p style={{ width : "280px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>SoPRO UV/LED Color Hybrid Gel *4 HEMA FREE 7 g</p>
            <p style={{width : "280px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>€9.49 </p>
          </div>
      </div>

      <div>
          <div style={{width : "300px" , height : "480px" }}>
            <img style={{height : "auto" , width : "300px"}} src={prod3} alt="logo" />
            <p style={{ width : "280px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>SoPRO UV/LED Color Hybrid Gel *4 HEMA FREE 7 g</p>
            <p style={{width : "280px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>€9.49 </p>
          </div>
      </div>

      <div>
          <div style={{width : "300px" , height : "480px" }}>
            <img style={{height : "auto" , width : "300px"}} src={prod4} alt="logo" />
            <p style={{ width : "280px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>SoPRO UV/LED Color Hybrid Gel *4 HEMA FREE 7 g</p>
            <p style={{width : "280px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>€9.49 </p>
          </div>
      </div>

      <div>
          <div style={{width : "300px" , height : "480px" }}>
            <img style={{height : "auto" , width : "300px"}} src={prod5} alt="logo" />
            <p style={{ width : "280px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>SoPRO UV/LED Color Hybrid Gel *4 HEMA FREE 7 g</p>
            <p style={{width : "280px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>€9.49 </p>
          </div>
      </div>

      <div>
          <div style={{width : "300px" , height : "480px" }}>
            <img style={{height : "auto" , width : "300px"}} src={prod6} alt="logo" />
            <p style={{ width : "280px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>SoPRO UV/LED Color Hybrid Gel *4 HEMA FREE 7 g</p>
            <p style={{width : "280px" ,margin: "auto" , fontSize: "16px" , marginTop : "10px"}}>€9.49 </p>
          </div>
      </div>

      </section>



      </section>
    )
  }
}
