import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


export default function SimpleAccordion() {
  return (
    <>
    
      <Accordion style={{
        boxShadow : "none" ,
        display : "flex" , 
        flexDirection : "column" , 
        alignItems: "normal",
        // borderBottom : "2px solid #2367FA",
        // borderRight : "2px solid #2367FA",
        // borderRadius : "7px",
        margin: 0

        }}>
        <AccordionSummary
          // expandIcon={}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography style={{
                color: "black",
                fontWeight: "600",
                fontSize: "18px",
                // borderTop: "2px solid transparent",
                transition: "all 220ms ease-in-out",
                width: "100%",
                display: "flex",
                justifyContent : "center",
          }}
          >CATEGORIE
          <ExpandMoreIcon 
          style={{
            color: "black",
            fontWeight: "600",
            fontSize: "30px",
            borderTop: "2px solid transparent",
            transition: "all 220ms ease-in-out"
          }}
          />
          </Typography>
        </AccordionSummary>
        <AccordionDetails style={{padding: "0"}}>
        <Link to="/categorie1" style={{textDecoration : "none"}}>
        
        <Typography style={{
                              cursor :"pointer" ,
                              fontWeight : "600" ,
                              color :"black" ,
                              backgroundColor: "#EDEDED",
                              width:  "95.5vw",
                              height: "50px",
                              display : "flex",
                              alignItems : "center",
                              justifyContent : "center",
                              // borderBottom : "2px solid #2367FA"
                               }}>
               Profile
          </Typography>
          </Link>
          
        </AccordionDetails>
        <AccordionDetails style={{padding: "0"}}>
        <Typography style={{
                              cursor :"pointer" ,
                              fontWeight : "600" ,
                              color :"black" ,
                              backgroundColor: "#EDEDED",
                              width:  "95.5vw",
                              height: "50px",
                              display : "flex",
                              alignItems : "center",
                              justifyContent : "center",
                              // borderBottom : "2px solid #2367FA"
                               }}>
               Sous categorie 2
          </Typography>
        </AccordionDetails>
        <AccordionDetails style={{padding: "0"}}>
        <Typography style={{
                              cursor :"pointer" ,
                              fontWeight : "600" ,
                              color :"black" ,
                              backgroundColor: "#EDEDED",
                              width:  "95.5vw",
                              height: "50px",
                              // borderRadius: "5px",
                              display : "flex",
                              alignItems : "center",
                              justifyContent : "center",
                               }}>
               Sous categorie 3
          </Typography>
        </AccordionDetails>
      </Accordion>
     
    </>
  );
}
