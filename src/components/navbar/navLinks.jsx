import { Stack } from "@mui/material";
import React, { useState ,useEffect  } from "react";
import styled from "styled-components";
import BasicMenu from "./categorie1/index";

const NavLinksContainer = styled.div`
  height: 100%;
  width : 80%;
  /* display: flex;
  align-items: center; */
  margin: auto;
  background-color : #fff;
`;

const LinksWrapper = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  height: 100%;
  list-style: none;
`;

const LinkItem = styled.li`
  height: 100%;
  padding: 0 1.1em;
  color: #2367FA;
  font-weight: 500;
  font-size: 14px;
  align-items: center;
  justify-content: center;
  display: flex;
  border-top: 2px solid transparent;
  transition: all 220ms ease-in-out;

  /* &:hover {
    border-top: 2px solid #133C99;
  } */
`;

const Link = styled.a`
  text-decoration: none;
  color: inherit;
  font-size: inherit;
`;



export function NavLinks(props) {

  useEffect(() => {
    window.addEventListener('scroll', handleScroll, { passive: true });

    return () => {
        window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const [scrollPosition, setScrollPosition] = useState(0);
  const handleScroll = () => {
    const position = window.pageYOffset;
    setScrollPosition(position);
  };

  return (
    <div style={ scrollPosition > 240 ? {width: "100%" ,backgroundColor : "#fff" ,position  : "fixed" , zIndex : "999999999" ,  top: "0px"} :  {width: "100%"  ,position  : "initial"}  }>
    <NavLinksContainer>
      <LinksWrapper>
        <LinkItem>
          {/* <Link href="#">About us</Link> */}
           <section href="#" className="ButtonStyle">
             HOME
           </section>
        </LinkItem>
        <LinkItem>
          {/* <Link href="#">About us</Link> */}
           <a style={{textDecoration :"none"}} href="#ABOUTUS" className="ButtonStyle">
             ABOUT US
           </a>
        </LinkItem>
        <LinkItem>
          {/* <Link href="#">About us</Link> */}
           <BasicMenu></BasicMenu>
        </LinkItem>
        <LinkItem>
          {/* <Link href="#">How it works</Link> */}
          <BasicMenu></BasicMenu>
        </LinkItem>
        <LinkItem>
          {/* <Link href="#">Explore</Link> */}
          <BasicMenu></BasicMenu>
        </LinkItem>
        <LinkItem>
          {/* <Link href="#">Impact</Link> */}
          <BasicMenu></BasicMenu>
        </LinkItem>
        <LinkItem>
          {/* <Link href="#">About us</Link> */}
           <a style={{textDecoration :"none"}} href="#CONTACT" className="ButtonStyle">
             CONTACT
           </a>
        </LinkItem>
      </LinksWrapper>

    </NavLinksContainer>
    </div>
  );
}
