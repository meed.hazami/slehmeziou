import React from "react";
import { useMediaQuery } from "react-responsive";
import styled from "styled-components";
import { Logo } from "../logo";
import { Accessibility } from "./accessibility";
import { NavLinks } from "./navLinks";
import { DeviceSize } from "../responsive";
import { MobileNavLinks } from "./mobileNavLinks";
import TextField from "@mui/material/TextField";
import InputAdornment from "@mui/material/InputAdornment";
import SearchIcon from "@mui/icons-material/Search";
import Button from '@mui/material/Button';
import { makeStyles } from "@material-ui/core/styles";
import CampaignIcon from '@mui/icons-material/Campaign';
import {
  Link
} from "react-router-dom";



import { StyledEngineProvider } from "@mui/material/styles";
import { createTheme } from "@mui/material/styles";
import { ThemeProvider } from "@mui/material/styles";

import Flags from "country-flag-icons/react/3x2";
import "./styleNavbar.css";

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  leftIcon: {
    marginRight: theme.spacing(1)
  },
  rightIcon: {
    marginLeft: theme.spacing(1)
  },
  iconSmall: {
    fontSize: 20
  }
}));

const theme = createTheme({
  // Components
  components: {
    MuiTextField: {
      defaultProps: {
        variant: "outlined",
        fullWidth: true,
        sx: { mb: 2 },
      },
      styleOverrides: {
        root: {
          marginTop: 40,

          // this is styles for the new variants
          "&.subvariant-hovered": {
            "& fieldset": {
              border: "none",
            },
            "& .MuiInputBase-input:hover + fieldset": {
              border: `2px solid blue`,
            },
            "& .MuiInputBase-input:focus + fieldset": {
              border: `2px solid blue`,
            },
          },
        },
      },
    },
  },
});

const NavbarContainer = styled.div`
  width: 100%;
  height: 60px;
  box-shadow: 0 1px 3px rgba(15, 15, 15, 0.13);
  display: flex;
  align-items: center;
  padding: 0 1.5em;
`;

const LeftSection = styled.div`
  display: flex;
`;

const MiddleSection = styled.div`
  display: flex;
  flex: 2;
  height: 100%;
  justify-content: center;
`;

const RightSection = styled.div`
  display: flex;
`;

export function Navbar(props) {
  const classes = useStyles();
  const isMobile = useMediaQuery({ maxWidth: DeviceSize.mobile });
  ////////////////////////////////////////////////////////////////////////////
  const [val, setVal] = React.useState(7);
  const [recherche, setRecherche] = React.useState(true);

  const handleChange = (event) => {
    setVal(event.target.value);
  };
  return (
    <>
      <NavbarContainer style={{ backgroundColor: "#000", height: "57px" }}>
        <LeftSection>
          <Accessibility
            value={val}
            handleChange={handleChange}
            items={[
              {
                key: "FR",
                value: 7,
                flagCon: (
                  <Flags.FR
                    style={{ width: "20px" }}
                    type="button"
                    title="United States"
                    className=""
                  />
                ),
              },
              {
                key: "EN",
                value: 28,
                flagCon: (
                  <Flags.GB
                    style={{ width: "20px" }}
                    type="button"
                    title="United States"
                    className=""
                  />
                ),
              },
            ]}
          />
        </LeftSection>

        <MiddleSection>
          <span
            style={{
              display: "flex",
              alignItems: "center",
              color: "#fff",
              fontSize: ".70em",
              letterSpacing: ".16em",
              lineHeight: "1.636em",
              fontWeight: "400",
              fontFamily: "'Open Sans',Arial,sans-serif",
            }}
          >
            DISCOVER NEW NAPPA SETS!{" "}
          </span>
        </MiddleSection>
        <RightSection></RightSection>
      </NavbarContainer>

      <NavbarContainer
        className="NavbarContainerStyle"
        style={{ backgroundColor: "#fff", height: "200px" }}
      >
        <LeftSection>
          {/* <section className="globalstyleNav" > */}
          <section style={{ display: "flex", alignItems: "center" }}>

            {isMobile && <MobileNavLinks />}

            {/* <SearchIcon
              className="iconeStyle"
              onClick={() => setRecherche(!recherche)}
            /> */}

            <section  style={{ display: "flex", flexWrap: "wrap" }}>
            <Link to="/">
                <Logo />
            </Link>
            </section>

          {/* </section> */}

          {/* <section
              className={recherche ? "TextFieldStyleRes" : "TextFieldStyle2"}
            > */}
            
              {/* <TextField
                autoComplete="off"
                id="outlined-basic"
                label="Recherche"
                // InputProps={{
                //   startAdornment: (
                //     <InputAdornment position="start">
                //       <SearchIcon />
                //     </InputAdornment>
                //   ),
                // }}
                size="small"
                variant="outlined"
                fullWidth
              /> */}

            
            {/* <Button
          variant="contained"
          color="inherit"
          className={classes.button}
        >
          nouveauté
          <CampaignIcon className={classes.rightIcon} />
        </Button> */}

            </section>

            {/* </section> */}
        </LeftSection>
        <MiddleSection></MiddleSection>
        <RightSection>
          {/* <section className={recherche ? "TextFieldStyle" : "TextFieldStyle1"}> */}
            {/* <TextField
              autoComplete="off"
              id="outlined-basic"
              label="Recherche"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
              size="small"
              variant="outlined"
            /> */}

        {/* <Button
          variant="contained"
          color="inherit"
          className={classes.button}
        >
          nouveauté
          <CampaignIcon className={classes.rightIcon} />
        </Button> */}

          {/* </section> */}
        </RightSection>
      </NavbarContainer>

      <NavbarContainer
        // className="NavbarContainerStyle1"
        // style={{ backgroundColor: "#fff", height: "80px", marginTop: "-50px" }}
      >
        <LeftSection></LeftSection>
        <MiddleSection>{!isMobile && <NavLinks />}</MiddleSection>
        <RightSection>{/* {!isMobile && <Accessibility />} */}</RightSection>
      </NavbarContainer>
    </>
  );
}
