// Accessibility


import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import ExpandMoreRoundedIcon from "@material-ui/icons/ExpandMoreRounded";

import Flags from 'country-flag-icons/react/3x2'

const useStyles = makeStyles(() => ({
  formControl: {
    "& .MuiInputBase-root": {
      color: "#fff",
      borderColor: "transparent",
      borderWidth: "1px",
      borderStyle: "solid",
      borderRadius: "100px",
      minWidth: "120px",
      justifyContent: "center"
    },
    "& .MuiSelect-select.MuiSelect-select": {
      display : "flex",
      paddingRight: "0px"
    }
  },
  select: {
    width: "auto",
    fontSize: "12px",
    "&:focus": {
      backgroundColor: "transparent"
    }
  },
  selectIcon: {
    top: "-1px",
    position: "relative",
    color: "#fff",
    fontSize: "14px"
  },
  paper: {
    borderRadius: 12,
    marginTop: 8
  },
  list: {
    paddingTop: 0,
    paddingBottom: 0,
    "& li": {
      fontWeight: 200,
      paddingTop: 8,
      paddingBottom: 8,
      fontSize: "12px"
    },
    "& li.Mui-selected": {
      color: "#000",
      background: "#FEE59F"
    },
    "& li.Mui-selected:hover": {
      background: "#c2ad73"
    }
  }
}));

export function Accessibility ({ value, handleChange ,items})  {
  const classes = useStyles();

  const menuProps = {
    classes: {
      list: classes.list,
      paper: classes.paper
    },
    anchorOrigin: {
      vertical: "bottom",
      horizontal: "center"
    },
    transformOrigin: {
      vertical: "top",
      horizontal: "center"
    },
    getContentAnchorEl: null
  };


  return (
    <FormControl className={classes.formControl}>
      <Select
        value={value}
        onChange={handleChange}
        disableUnderline
        IconComponent={ExpandMoreRoundedIcon}
        MenuProps={menuProps}
        classes={{
          select: classes.select,
          icon: classes.selectIcon
        }}
      >
        {items.map((item) => (
          <MenuItem key={item.key} value={item.value}>

            <span>{item.key}</span>
            
            <section style={{marginLeft : "5px"}}>
            {item.flagCon}
            </section>

          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};
