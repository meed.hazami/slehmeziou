import "./App.css";
import { Navbar } from "./components/navbar";
import Home from "./components/body/index"
import Categoriecom1 from "./components/categorie1Com"
import Section6 from "./components/body/Section6"
import Produitinfo from "./components/produitinfo"

import { BrowserRouter as Router, Link, Route, Routes } from 'react-router-dom'

function App() {
  return (
    <div className="App">
      
        
      
    
    <Router>
      <Navbar/>
      <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/categorie1" element={<Categoriecom1/>}/>
          <Route path="/produitInfo/:text/:img" element={<Produitinfo/>}/>
      </Routes>
      <Section6/>
    </Router>
    </div>
  );
}

export default App;
